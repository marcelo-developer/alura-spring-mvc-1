<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<base href="http://localhost:8080/casadocodigo/">
	<title>Produtos - Incluir</title>
</head>
<body>
	<h1>Incluir produto</h1>
	<form:form action="${s:mvcUrl('PC#gravar').build()}" method="post" commandName="produto" enctype="multipart/form-data">
		<div>
			<label>Título</label><br>
			<form:input path="titulo" /><br>
			<form:errors path="titulo" />
		</div>
		<div>
			<label>Descrição</label><br>
			<form:textarea rows="10" cols="20" path="descricao" /><br>
			<form:errors path="descricao" />
		</div>
		<div>
			<label>Páginas</label><br>
			<form:input path="paginas" /><br>
			<form:errors path="paginas" />
		</div>
		<div>
			<label>Data de lançamento</label><br>
			<form:input path="dataLancamento" /><br>
			<form:errors path="dataLancamento" />
		</div>
		<br>
		<c:forEach items="${tipos}" var="tipoPreco" varStatus="status">
			<div>
				<label>${tipoPreco}</label><br>
				<form:input path="precos[${status.index}].valor" /><br>
				<form:hidden path="precos[${status.index}].tipo" value="${tipoPreco}" />
			</div>
		</c:forEach>
		<br>
		<div>
			<label>Sumário</label><br>
			<input name="sumario" type="file">
		</div>
		<button type="submit">Cadastrar</button>
	</form:form>
	<nav>
		<p><a href="produtos">Produtos</a></p>
		<p><a href="">Início</a></p>
	</nav>
</body>
</html>