<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<html>
<head>
	<meta charset="UTF-8">
	<base href="http://localhost:8080/casadocodigo/">
	<title>Produtos - Lista</title>
	<style>
		td, th {
			padding: 0.5em;
			text-align: left;
		}
	</style>
</head>
<body>
	<h1>Lista de produtos</h1>
	<h2>${mensagem}</h2>
	<table>
		<tr>
			<th>Título</th>
			<th>Descrição</th>
			<th>Páginas</th>
			<th>Lançamento</th>
		</tr>
		<c:forEach items="${produtos}" var="produto">
			<tr>
				<td>
					<a href="${s:mvcUrl('PC#detalhe').arg(0, produto.id).build()}">${produto.titulo}</a></td>
				<td>${produto.descricao}</td>
				<td>${produto.paginas}</td>
				<td>${produto.dataLancamentoFormatada}</td>
			</tr>
		</c:forEach>
	</table>
	<nav>
		<a href="produtos/form">Incluir</a>
	</nav>
</body>
</html>