<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Home</title>
</head>
<body>
	<h1>Casa do Código</h1>
	<table>
		<tr>
			<td>Java 8 Prático</td>
			<td>Certificado OCJP</td>
		</tr>
		<tr>
			<td>TDD na Prática para Java</td>
			<td>Google Android</td>
		</tr>
	</table>
	<br>
	<h2>Menu</h2>
	<nav>
		<a href="produtos">Produtos</a>
	</nav>
</body>
</html>